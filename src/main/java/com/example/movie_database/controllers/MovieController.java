package com.example.movie_database.controllers;


import com.example.movie_database.models.Character;
import com.example.movie_database.models.Franchise;
import com.example.movie_database.models.Movie;
import com.example.movie_database.repositories.CharacterRepository;
import com.example.movie_database.repositories.MovieRepository;
import com.example.movie_database.services.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/movies")
public class MovieController {

    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;

    }

    //List all movies
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        return movieService.getAllMovies();
    }

    //Add a new movie
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        return movieService.addMovie(movie);
    }

    //Display a movie based on its ID
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById (@PathVariable Long id) {
        return movieService.getMovieById(id);
    }

    //Update a movie
    @PutMapping("{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movieDetails) {
        return movieService.updateMovie(id, movieDetails);

    }

    //Delete a movie based on its ID
    @DeleteMapping("{id}")
    public String deleteMovie(@PathVariable Long id, @RequestBody Movie movie){
        return movieService.deleteMovie(id, movie);
    }
}


