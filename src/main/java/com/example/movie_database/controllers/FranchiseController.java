package com.example.movie_database.controllers;

import com.example.movie_database.models.Character;
import com.example.movie_database.models.Franchise;
import com.example.movie_database.models.Movie;
import com.example.movie_database.repositories.FranchiseRepository;
import com.example.movie_database.repositories.MovieRepository;
import com.example.movie_database.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;

    public FranchiseController(FranchiseService franchiseService) {
        this.franchiseService = franchiseService;
    }

    //List all franchises
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        return franchiseService.getAllFranchises();
    }

    //Add a new franchise
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        return franchiseService.addFranchise(franchise);
    }

    //Display a franchise based on its ID
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchiseById (@PathVariable Integer id) {
        return franchiseService.getFranchiseById(id);
    }

    //Update a franchise
    @PutMapping("{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Integer id, @RequestBody Franchise franchiseDetails) {
        return franchiseService.updateFranchise(id, franchiseDetails);

    }

    //Delete a franchise based on its ID
    @DeleteMapping("{id}")
    public String deleteFranchise(@PathVariable Integer id, @RequestBody Franchise franchise){
        return franchiseService.deleteFranchise(id, franchise);
    }
}
