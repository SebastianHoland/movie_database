package com.example.movie_database.controllers;

import com.example.movie_database.models.Character;
import com.example.movie_database.services.CharacterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/characters")
public class CharacterController {
    private final CharacterService characterService;

    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    //List all characters
    @GetMapping()
    public ResponseEntity<List<Character>> getAllCharacters() {
       return characterService.getAllCharacters();
    }

    //Add a new character
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        return characterService.addCharacter(character);
    }

    //Display a character based on their ID
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacterById (@PathVariable Long id) {
        return characterService.getCharacterById(id);
    }

    //Delete a character based on their ID
    @DeleteMapping("{id}")
    public ResponseEntity<Character> deleteCharacter(@PathVariable Long id) {
        return characterService.deleteCharacter(id);
    }

    //Update a character
    @PutMapping("{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character characterDetails) {
        return characterService.updateCharacter(id, characterDetails);

    }
}
