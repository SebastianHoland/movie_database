package com.example.movie_database.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    @Column(length = 60)
    public String title;
    @Column(length = 60)
    private String genre;
    @Column(length = 20)
    private int releaseYear;
    @Column(length = 60)
    private String director;
    @Column(length = 80)
    private String pictureURL;
    @Column(length = 80)
    private String trailerURL;

    //Relationships
    @ManyToOne
    private Franchise franchise;



    @ManyToMany (mappedBy = "movies")
    private Set<Character> characters;

    public Movie() {
    }

    public Movie(long id, String title, String genre, int releaseYear, String director, String pictureURL, String trailerURL, Franchise franchise, Set<Character> characters) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.pictureURL = pictureURL;
        this.trailerURL = trailerURL;
        this.franchise = franchise;
        this.characters = characters;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getTrailerURL() {
        return trailerURL;
    }

    public void setTrailerURL(String trailerURL) {
        this.trailerURL = trailerURL;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    @JsonGetter("characters")
    public List<Long> getCharacters() {
        if(characters == null)
            return null;
        return characters.stream().map(m -> m.getId()).collect(Collectors.toList());
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }
}


