package com.example.movie_database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.movie_database.models.Character;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
}
