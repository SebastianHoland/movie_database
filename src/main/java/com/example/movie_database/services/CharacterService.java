package com.example.movie_database.services;

import com.example.movie_database.models.Character;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CharacterService {
    public ResponseEntity<List<Character>> getAllCharacters();
    public ResponseEntity<Character> addCharacter(Character character);
    public ResponseEntity<Character> getCharacterById (Long id);
    public ResponseEntity<Character> updateCharacter(Long id, Character characterDetails);
    public ResponseEntity<Character> deleteCharacter(Long id);
}
