package com.example.movie_database.services;

import com.example.movie_database.models.Movie;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MovieService {
    public ResponseEntity<List<Movie>> getAllMovies();
    public ResponseEntity<Movie> addMovie(Movie movie);
    public ResponseEntity<Movie> getMovieById (Long id);
    public ResponseEntity<Movie> updateMovie(Long id, Movie movieDetails);
    public String deleteMovie(Long id, Movie movie);


}
