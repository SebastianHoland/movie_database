package com.example.movie_database.services;

import com.example.movie_database.models.Character;
import com.example.movie_database.repositories.CharacterRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    //List all characters
    public ResponseEntity<List<Character>> getAllCharacters() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characterRepository.findAll(), status);
    }

    //Add a new character
    public ResponseEntity<Character> addCharacter(Character character) {
        return new ResponseEntity<>(characterRepository.save(character), HttpStatus.CREATED);
    }

    //Get a character based on their ID
    public ResponseEntity<Character> getCharacterById (Long id) {
        Character character = new Character();
        HttpStatus status;

        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            character = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(character, status);
    }

    //Delete a character based on their ID
    public ResponseEntity<Character> deleteCharacter (Long id) {
        HttpStatus status;

        if (!characterRepository.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            characterRepository.deleteById(id);
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(status);
    }

    //Update a character
    public ResponseEntity<Character> updateCharacter(Long id, Character characterDetails) {
        Character returnCharacter = new Character();
        HttpStatus status;

        //For some reason the ID is set to 0 even though I specify the ID.
        if(!id.equals(characterDetails.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter, status);
        }

        returnCharacter = characterRepository.save(characterDetails);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);

    }




}
