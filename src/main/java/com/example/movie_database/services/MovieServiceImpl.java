package com.example.movie_database.services;

import com.example.movie_database.models.Character;
import com.example.movie_database.models.Movie;
import com.example.movie_database.repositories.CharacterRepository;
import com.example.movie_database.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService{
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    //List all movies
    public ResponseEntity<List<Movie>> getAllMovies() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieRepository.findAll(), status);
    }

    //Add a new movie
    public ResponseEntity<Movie> addMovie(Movie movie) {
        return new ResponseEntity<>(movieRepository.save(movie), HttpStatus.CREATED);
    }

    //Get a movie based on its ID
    public ResponseEntity<Movie> getMovieById (Long id) {
        Movie movie = new Movie();
        HttpStatus status;

        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            movie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(movie, status);
    }

    //Update a movie
    public ResponseEntity<Movie> updateMovie(Long id, Movie movieDetails) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        if(!id.equals(movieDetails.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }

        returnMovie = movieRepository.save(movieDetails);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);

    }

    //Delete a movie based on its ID
    public String deleteMovie(Long id, Movie movie){

        if(!id.equals(movie.getId())){
            return "something went wrong";
        }

        List<Character> allCharacters = characterRepository.findAll();

        for(Character character: allCharacters){
            Set<Movie> movieToCharacter = character.getMovies();
            if(movieToCharacter == null){
                continue;
            }else {
                for(Movie movie1 : movieToCharacter){
                    if(movie1.getId() == id){
                        movieToCharacter.remove(movie1);
                    }
                }
                character.setMovies(movieToCharacter);
            }
        }

        movieRepository.deleteById(id);
        return "movie deleted";
    }

}
