package com.example.movie_database.services;

import com.example.movie_database.models.Franchise;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface FranchiseService {
    public ResponseEntity<List<Franchise>> getAllFranchises();
    public ResponseEntity<Franchise> addFranchise(Franchise franchise);
    public ResponseEntity<Franchise> getFranchiseById (Integer id);
    public ResponseEntity<Franchise> updateFranchise(Integer id, Franchise franchiseDetails);
    public String deleteFranchise(Integer id, Franchise franchise);
}
