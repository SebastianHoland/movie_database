package com.example.movie_database.services;

import com.example.movie_database.models.Franchise;
import com.example.movie_database.models.Movie;
import com.example.movie_database.repositories.FranchiseRepository;
import com.example.movie_database.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FranchiseServiceImpl implements FranchiseService {
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    //List all franchises
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchiseRepository.findAll(), status);
    }

    //Add a new franchise
    public ResponseEntity<Franchise> addFranchise(Franchise franchise) {
        return new ResponseEntity<>(franchiseRepository.save(franchise), HttpStatus.CREATED);
    }

    //Get a franchise based on its ID
    public ResponseEntity<Franchise> getFranchiseById (Integer id) {
        Franchise franchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(franchise, status);
    }

    //Update a franchise
    public ResponseEntity<Franchise> updateFranchise(Integer id, Franchise franchiseDetails) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if(!id.equals(franchiseDetails.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }

        returnFranchise = franchiseRepository.save(franchiseDetails);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);

    }

    //Delete a franchise
    public String deleteFranchise(Integer id, Franchise franchise){

        if(!id.equals(franchise.getId())){
            return "something wrong";
        }

        List<Movie> allMovies = movieRepository.findAll();

        for(Movie movie: allMovies){
            if(movie.getFranchise() == null){
                continue;
            }else if(movie.getFranchise().getId() == id){
                movie.setFranchise(null);
            }
        }

        franchiseRepository.deleteById(id);
        return "The franchise is deleted";
    }


}
