INSERT INTO franchise (name, description) VALUES ('SAW', 'Stupid people that cant work together');
INSERT INTO franchise (name, description) VALUES ('LOTR', 'Too much work over a ring');

INSERT INTO movie (title, genre, release_year, franchise_id) VALUES('Saw 1', 'Horror', 2000, 1);
INSERT INTO movie (title, genre, release_year, franchise_id) VALUES('Saw 2', 'Horror', 2002, 1);
INSERT INTO movie (title, genre, release_year, franchise_id) VALUES('Saw 3', 'Horror', 2004, 1);

INSERT INTO movie (title, genre, release_year, franchise_id) VALUES('LOTR 1', 'Adventure', 2006, 2);
INSERT INTO movie (title, genre, release_year, franchise_id) VALUES('LOTR 2', 'Adventure', 2008, 2);
INSERT INTO movie (title, genre, release_year, franchise_id) VALUES('LOTR 3', 'Adventure', 2010, 2);



INSERT INTO character (full_name, alias, gender) VALUES('Gordon', 'Shortlegs', 'Non-Binary');
INSERT INTO character (full_name, alias, gender) VALUES('Paul', 'Longlegs', 'Male');
INSERT INTO character (full_name, alias, gender) VALUES('Jigsaw', 'Shortlegs', 'Female');

INSERT INTO movie_character (character_id, movie_id) VALUES (1, 1);
INSERT INTO movie_character (character_id, movie_id) VALUES (2, 2);
INSERT INTO movie_character (character_id, movie_id) VALUES (3, 3);
