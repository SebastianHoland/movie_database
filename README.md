## Assignment 3 in the javacourse at Noroff
Create a PostgreSQL database using Hibernate and expose it through a Web API.

## Project Description
In this project I have created controllers, models, repositories and services to to be able to display, and have full CRUD functionality towards a movie database. 

## Problems
I had trouble completing all the required functionality so the project is lacking some of the functionality.
THe missing functionality is: "Get all the movies in a franchise", "Get all the characters in a movie" and "Get all the characters in a franchise".

## Author
Paul Sebastian Hornnes Holand (@SebastianHoland)

## Swaggerlink
http://localhost:8080/swagger-ui/index.html
